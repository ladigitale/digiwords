import { createRouter, createWebHashHistory } from 'vue-router'
import Accueil from '../views/Accueil.vue'
import Nuage from '../views/Nuage.vue'

const routes = [
	{
		path: '/',
		name: 'Accueil',
		component: Accueil
	},
	{
		path: '/n/:id',
		name: 'Nuage',
		component: Nuage
	}
]

const router = createRouter({
	history: createWebHashHistory(),
	routes
})

export default router
