<?php

session_start();

require 'headers.php';

if (!empty($_POST['id'])) {
	require 'db.php';
	$reponse = '';
	$id = $_POST['id'];
	if (isset($_SESSION['digiwords'][$id]['reponse'])) {
		$reponse = $_SESSION['digiwords'][$id]['reponse'];
	}
	$stmt = $db->prepare('SELECT * FROM digiwords_nuages WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($nuage = $stmt->fetchAll()) {
			$admin = false;
			if (count($nuage, COUNT_NORMAL) > 0 && $nuage[0]['reponse'] === $reponse) {
				$admin = true;
			}
			$donnees = $nuage[0]['donnees'];
			if ($donnees !== '') {
				$donnees = json_decode($donnees);
			}
			$digidrive = 0;
			if (isset($_SESSION['digiwords'][$id]['digidrive'])) {
				$digidrive = $_SESSION['digiwords'][$id]['digidrive'];
			} else if (intval($nuage[0]['digidrive']) === 1) {
				$digidrive = 1;
			}
			$date = date('Y-m-d H:i:s');
			$vues = 0;
			if ($nuage[0]['vues'] !== '') {
				$vues = intval($nuage[0]['vues']);
			}
			if ($admin === false) {
				$vues = $vues + 1;
			}
			$stmt = $db->prepare('UPDATE digiwords_nuages SET vues = :vues, derniere_visite = :derniere_visite WHERE url = :url');
			if ($stmt->execute(array('vues' => $vues, 'derniere_visite' => $date, 'url' => $id))) {
				echo json_encode(array('nom' => $nuage[0]['nom'], 'donnees' => $donnees, 'vues' => $vues, 'admin' =>  $admin, 'digidrive' => $digidrive));
			} else {
				echo 'erreur';
			}
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
